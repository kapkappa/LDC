#include "defs.h"
#include "solver.h"
#include "field.h"
#include "psolver.h"
#include "logging.hpp"

#include "XAMG/src/init.h"

#include <iostream>
#include <cmath>
#include <cassert>

namespace {

double norm(const Field& A, const Field& B) {   //L2-norm
    assert(A.get_total_size() == B.get_total_size());
    assert(A.is_initialized() && B.is_initialized());

    double result = 0.0;
    for (int i = 0; i < A.get_total_size(); i++) {
        double tmp = B[i] - A[i];
        result += tmp * tmp;
    }
//    std::cout << "result: " << result << std::endl;
    return std::sqrt(result);
}

static inline double timer() {
    struct timeval tp;
    struct timezone tzp;
    gettimeofday(&tp, &tzp);
    return ((double)tp.tv_sec + (double)tp.tv_usec * 1.e-6);
}

double update_dt(double dt, int iters, int N) {
    double max_dt = 10.0 / (N * N);
//    double max_dt = 5e-4;

// ranges: [1..2], [3..4], [5..7], [8..12], [13..+oo]
    if (iters <= 2 && dt < max_dt)
        dt *= 1.15;
    else if (3 <= iters && iters <= 4 && dt < max_dt)
        dt *= 1.05;
    else if (5 <= iters && iters <= 7)
        dt *= 1.0;
    else if (8 <= iters && iters <= 12)
        dt *= 0.9;
    else if (iters > 12)
        dt *= 0.8;

    return dt;
}

} //namespace

void Solver::init_psolver() {
    int argc = 1;
    char* argv[] = {"prog", NULL};
    XAMG::init(argc, argv, "nnumas=1:ncores=1");
}

void Solver::finalize_psolver() {
    XAMG::finalize();
}

void Solver::initialize() {
// Boundary conditions
    U.init(N+1, N);
    V.init(N, N+1);
    P.init(N, N);

    U_inaccurate = U;
    V_inaccurate = V;

    U_next = U;
    V_next = V;

    U_extra = U;
    V_extra = V;

    U_star = U;
    V_star = V;
}

void Solver::calculate_U_star(const Field& Utemp, const Field& Vtemp) {
    for (int j = 0; j < N; j++) {
        for (int i = 1; i < N; i++) {
            U_star[i + j * (N+1)] = U[i + j * (N+1)] + (-Conv_x(i, j, Utemp, Vtemp) + Visc_x(i, j, Utemp)) * dt / (dx * dx);
        }
    }
    for (int j = 0; j < N; j++) {
        U_star[0 + j * (N+1)] = 0.0;
        U_star[N + j * (N+1)] = 0.0;
    }
}

void Solver::calculate_V_star(const Field& Utemp, const Field& Vtemp) {
    for (int j = 1; j < N; j++) {
        for (int i = 0; i < N; i++) {
            V_star[i + j * N] = V[i + j * N] + (-Conv_y(i, j, Utemp, Vtemp) + Visc_y(i, j, Vtemp)) * dt / (dx * dx);
        }
    }
    for (int i = 0; i < N; i++) {
        V_star[i + 0 * N] = 0.0;
        V_star[i + N * N] = 0.0;
    }
}

void Solver::calculate_U_extra() {

    for(int j = 0; j < N; ++j) {
        for(int i = 1; i < N; ++i) {
            U_extra[i + j * (N+1)] = -(P[i + j * N] - P[i-1 + j * N]) * dt / dx;
        }
    }

    for (int j = 0; j < N; ++j) {
        U_extra[0 + j * (N+1)] = 0.0;
        U_extra[N + j * (N+1)] = 0.0;
    }
}

void Solver::calculate_V_extra() {

    for(int j = 1; j < N; ++j) {
        for(int i = 0; i < N; ++i) {
            V_extra[i + j * N] = -(P[i + j * N] - P[i + (j-1) * N]) * dt / dy;
        }
    }

    for (int i = 0; i < N; ++i) {
        V_extra[i + 0 * N] = 0.0;
        V_extra[i + N * N] = 0.0;
    }
}


double Solver::Conv_x (int i, int j, const Field& Utemp, const Field& Vtemp) const {
//    return 0.0;
    double result;
    double f1, f2, f3, f4;                  // Flow quantity: [i+1/2 i-1/2 j+1/2 j-1/2]
    double uds_f1, uds_f2, uds_f3, uds_f4;  // Dependent quantity, U (or Phi in literature)
    double U_n, U_s, U_w, U_e, U_p;         // top, bottom, left, right, center ~ north, south, west, east, centerpoint
    double V_tl, V_tr, V_bl, V_br;          // top-left, top-right, bottom-left, bottomm-right

    assert(i != 0 && i != N);

    U_w = Utemp[i-1 + j*(N+1)];
    U_e = Utemp[i+1 + j*(N+1)];
    if (j != 0)   U_s = Utemp[i + (N+1)*(j-1)]; else U_s = -Utemp[i + (N+1)*j];
    if (j != N-1) U_n = Utemp[i + (N+1)*(j+1)]; else U_n = 2 - Utemp[i + (N+1)*j];

    U_p = Utemp[i + (N+1)*j];

    V_tl = Vtemp[i-1 + (j+1)*N];
    V_bl = Vtemp[i-1 + j*N];
    V_tr = Vtemp[i + (j+1)*N];
    V_br = Vtemp[i + j*N];

    f1 = (U_p + U_e) / 2;
    f2 = (U_w + U_p) / 2;
    f3 = (V_tl + V_tr) / 2;
    f4 = (V_bl + V_br) / 2;

    if (f1>=0) uds_f1 = U_p; else uds_f1 = U_e;
    if (f2>=0) uds_f2 = U_w; else uds_f2 = U_p;
    if (f3>=0) uds_f3 = U_p; else uds_f3 = U_n;
    if (f4>=0) uds_f4 = U_s; else uds_f4 = U_p;

    result = dx * (f1*uds_f1 - f2*uds_f2) + dy * (f3*uds_f3 - f4*uds_f4);
    return result;
}


double Solver::Conv_y (int i, int j, const Field& Utemp, const Field& Vtemp) const {
//    return 0.0;
    double result;
    double f1, f2, f3, f4;                  // Flow quantity: [i+1/2 i-1/2 j+1/2 j-1/2]
    double uds_f1, uds_f2, uds_f3, uds_f4;  // Dependent quantity, V (or Phi in literature)
    double U_tl, U_bl, U_tr, U_br;          // top-left, bottom-left, top-right, bottom-right
    double V_n, V_s, V_w, V_e, V_p;         // top, bottom, left, right, center ~ north, south, west, east, centerpoint

    assert(j != 0 && j != N);

    U_bl = Utemp[i + (j-1)*(N+1)];
    U_br = Utemp[i+1 + (j-1)*(N+1)];
    U_tl = Utemp[i + j*(N+1)];
    U_tr = Utemp[i+1 + j*(N+1)];


    if (i != 0)   V_w = Vtemp[i-1 + j*N];   else V_w = -Vtemp[i + j*N];
    if (i != N-1) V_e = Vtemp[i+1 + j*N];   else V_e = -Vtemp[i + j*N];
    V_s = Vtemp[i + N*(j-1)];
    V_n = Vtemp[i + N*(j+1)];
    V_p = Vtemp[i + N*j];

    f1 = (U_tr + U_br) / 2;
    f2 = (U_tl + U_bl) / 2;
    f3 = (V_p + V_n) / 2;
    f4 = (V_s + V_p) / 2;

    if (f1>=0) uds_f1 = V_p; else uds_f1 = V_e;
    if (f2>=0) uds_f2 = V_w; else uds_f2 = V_p;
    if (f3>=0) uds_f3 = V_p; else uds_f3 = V_n;
    if (f4>=0) uds_f4 = V_s; else uds_f4 = V_p;

    result = dx * (f1*uds_f1 - f2*uds_f2) + dy * (f3*uds_f3 - f4*uds_f4);
    return result;
}

double Solver::Visc_x (int i, int j, const Field& Utemp) const {
    double result;
    double U_t, U_b, U_l, U_r, U_c; // top, bottom, left, right, center

    assert(i != 0 && i != N);

    U_l = Utemp[i-1 + j*(N+1)];
    U_r = Utemp[i+1 + j*(N+1)];
    if (j != 0)   U_b = Utemp[i + (N+1)*(j-1)]; else U_b = -Utemp[i + (N+1)*j];
    if (j != N-1) U_t = Utemp[i + (N+1)*(j+1)]; else U_t = 2 - Utemp[i + (N+1)*j];
    U_c = Utemp[i + (N+1)*j];

    result = U_r + U_l + U_t + U_b - 4*U_c;
    return result / Re; // take into accout Reynold's number
}

double Solver::Visc_y (int i, int j, const Field& Vtemp) const {
    double result;
    double V_t, V_b, V_l, V_r, V_c; // top, bottom, left, right, center

    assert(j != 0 && j != N);

    if (i != 0)   V_l = Vtemp[i-1 + j*N];   else V_l = -Vtemp[i + j*N];
    if (i != N-1) V_r = Vtemp[i+1 + j*N];   else V_r = -Vtemp[i + j*N];
    V_b = Vtemp[i + N*(j-1)];
    V_t = Vtemp[i + N*(j+1)];
    V_c = Vtemp[i + N*j];

    result = V_r + V_l + V_t + V_b - 4*V_c;
    return result / Re; // take into accout Reynold's number
}


void Solver::solve() {
    double t = 0.0, solver_time = 0.0;
    int iter = 0;
    double residual = 1.0;

    Psolver psolver(N, dx, dy);

    while (residual >= global_tolerance && iter < max_timesteps) {
        calculate_U_star(U, V); //U* = Un + dt/dh^2 * (-Conv_x + Visc_x)
        calculate_V_star(U, V); //V* = Vn + dt/dh^2 * (-Conv_y + Visc_y)

        double tt1 = timer();

        psolver.solve(U_star, V_star, P, dt); //Create RHS; Solve Puasson equation; Store result in P

        double tt2 = timer();
        solver_time += tt2 - tt1;

        calculate_U_extra(); // U' = dt/dh^2 * Px
        calculate_V_extra(); // V' = dt/dh^2 * Py

        U_next = U_star + U_extra;
        V_next = V_star + V_extra;

        double linear_residual = std::max(norm(U_next, U), norm(V_next, V));

        int linear_iter = 0;

        while (linear_residual >= clarification_tolerance) {
            U_inaccurate = U_next;
            V_inaccurate = V_next;

            calculate_U_star(U_inaccurate, V_inaccurate);
            calculate_V_star(U_inaccurate, V_inaccurate);

            double tt1 = timer();
            psolver.solve(U_star, V_star, P, dt);
            double tt2 = timer();
            solver_time += tt2 - tt1;

            calculate_U_extra();
            calculate_V_extra();

            U_next = U_star + U_extra;
            V_next = V_star + V_extra;

            linear_residual = std::max(norm(U_inaccurate, U_next), norm(V_inaccurate, V_next));
            linear_iter += 1;
//            std::cout << "Linear iter: " << linear_iter << "\tLinear residual: " << linear_residual << std::endl;
        }

        t += dt;

        residual = std::max(norm(U_next, U) / dt, norm(V_next, V) / dt);

        dt = update_dt(dt, linear_iter, N);

        iter++;

        U = U_next;
        V = V_next;

        if (iter % 20000 == 0) {
            logging::write_horizontal_profile(U, "profiles/U_"+std::to_string(iter), t);
            logging::write_vertical_profile(V, "profiles/V_"+std::to_string(iter), t);
        }

        if (iter % 1000 == 0)
            std::cout << "Iter: " << iter << "\tCurrent time: " << t << "\tdt: " << dt << "\tResidual: " << residual << "\tlinear iters: " << linear_iter << std::endl;

    }

    std::cout << "Total time: " << t << std::endl;
    std::cout << "Solver time: " << solver_time << std::endl;

    logging::write_field(U, "U", t);
    logging::write_field(V, "V", t);
}
