#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import sys
from numpy import linalg as LA
from decimal import Decimal

# Input
if len(sys.argv) != 5:
    print("Need args: Re N dt ntimesteps")
    sys.exit(1)

Re = sys.argv[1]
N = sys.argv[2]
dt = sys.argv[3]
n_iters = sys.argv[4]


# Supporting functions
def calculate_centered_U(n_size, A):
    U_centered = np.zeros(shape = (n_size, n_size))
    for i in range(n_size):
        U_centered[:,i] = (A[:,i]+A[:,i+1])/2.0
    return U_centered

def calculate_centered_V(n_size, A):
    V_centered = np.zeros(shape = (n_size, n_size))
    for i in range(n_size):
        V_centered[i, :] = (A[i, :]+A[i+1, :])/2.0
    return V_centered

def print_vorticity(U, V, N):
    #vorticity = dv/dx - du/dy
    vx = V.shape[0] // 2
    vy = V.shape[1] // 2
    ux = U.shape[0] // 2
    uy = U.shape[1] // 2
    vorticity = (V[vx][vy + 1] + V[vx][vy] - U[ux + 1][uy] - U[ux][uy]) / int(N)
    print("vorticity: {}".format(vorticity))

def get_norm(X_val, Y_val, U_val, V_val, X, Y, U, V):
    res = []

    X_val_indices = [list(X).index(min(X, key=lambda b:abs(b-a))) for a in X_val]
    Y_val_indices = [list(Y).index(min(Y, key=lambda b:abs(b-a))) for a in Y_val]

    U_res = [U[i] for i in Y_val_indices]
    V_res = [V[i] for i in X_val_indices]

    res.append(LA.norm(np.array(U_res)-np.array(U_val)))
    res.append(LA.norm(np.array(V_res)-np.array(V_val)))
    return res



# Get values
U = np.fromfile("U", dtype=np.double)
V = np.fromfile("V", dtype=np.double)

time = int(U[0])

uy = int(U[1])
ux = int(U[2])
U = U[3:]

vy = int(V[1])
vx = int(V[2])
V = V[3:]

if (vy != ux or vx != uy):
    print("ERROR")
    exit(1)


# Reshape
U = U.reshape(uy, ux)
V = V.reshape(vy, vx)


print_vorticity(U, V, N)


X = np.linspace(0, 1, vx)
Y = np.linspace(0, 1, uy)


# Centering field
U_cent = calculate_centered_U(U.shape[0], U)
V_cent = calculate_centered_V(V.shape[1], V)


# Get profiles in the geometric center of the cavity
U_profile = U_cent[:, U_cent.shape[1]//2]
V_profile = V_cent[V_cent.shape[0]//2, :]


fig, ax = plt.subplots(1,4)
#fig.tight_layout(h_pad=1)


# Plot streamfunction and quiver field
ax[0].streamplot(X, Y, U_cent, V_cent, linewidth=0.5, minlength=0.7, maxlength=5.0, density=1.0, broken_streamlines=False)
ax[0].set_title('Streamplot')

ax[1].quiver(X, Y, U_cent, V_cent, np.sqrt(U_cent**2+V_cent**2), cmap='inferno')
ax[1].set_title('Quiver plot')


ax[2].plot(U_profile, Y, color='blue', label="time: "+str(time))
ax[2].set_title('Horizontal velocity profile')

ax[3].plot(X, V_profile, color='blue', label="time: "+str(time))
ax[3].set_title('Vertical velocity profile')


# Reference values for different Re numbers
if (int(Re) == 400):
    Y_val = [0.0000, 0.0547, 0.0625, 0.0703, 0.1016, 0.1719, 0.2813, 0.4531, 0.5000,
             0.6172, 0.7344, 0.8516, 0.9531, 0.9609, 0.9688, 0.9766, 1.0000]
    X_val = [0.0000, 0.0625, 0.0703, 0.0781, 0.0938, 0.1563, 0.2266, 0.2344, 0.5000,
             0.8047, 0.8594, 0.9063, 0.9453, 0.9531, 0.9609, 0.9688, 1.0000]

    U_val = [0.00000, -0.08186, -0.09266, -0.10338, -0.14612, -0.24299, -0.32726, -0.17119, -0.11477,
             0.02135, 0.16256, 0.29093, 0.55892, 0.61756, 0.68439, 0.75837, 1.00000]
    V_val = [0.00000, 0.18360, 0.19713, 0.20920, 0.22965, 0.28124, 0.30203, 0.30174, 0.05186,
             -0.38598, -0.44993, -0.23827, -0.22847, -0.19254, -0.15663, -0.12146, 0.00000]

    norms = get_norm(X_val, Y_val, U_val, V_val, X, Y, U_profile, V_profile)

    ax[2].plot(U_val, Y_val, '--o', color='black', label="Ghia1982, ref values")
    ax[3].plot(X_val, V_val, '--o', color='black', label="Ghia1982, ref values")
    ax[2].set_title("Horizontal velocity profile\nResidual: {:.2E}".format(Decimal(norms[0])))
    ax[3].set_title("Vertical velocity profile\nResidual: {:.2E}".format(Decimal(norms[1])))


if (int(Re) == 2500):
    U_val = [0.0000, -0.1517, -0.2547, -0.3372, -0.3979, -0.4250, -0.4200, -0.3965, -0.3688, -0.3439, -0.3228, -0.0403,
             0.4141, 0.4256, 0.4353, 0.4424, 0.4470, 0.4506, 0.4607, 0.4971, 0.5924, 0.7704, 1.0000]
    V_val = [0.0000, 0.1607, 0.2633, 0.3238, 0.3649, 0.3950, 0.4142, 0.4217, 0.4187, 0.4087, 0.3918, 0.0160,
             -0.3671, -0.3843, -0.4042, -0.4321, -0.4741, -0.5268, -0.5603, -0.5192, -0.3752, -0.1675, 0.0000]

    Y_val = [0.000, 0.020, 0.040, 0.060, 0.080, 0.100, 0.120, 0.140, 0.160, 0.180, 0.200, 0.500,
             0.900, 0.910, 0.920, 0.930, 0.940, 0.950, 0.960, 0.970, 0.980, 0.990, 1.000]
    X_val = [0.000, 0.015, 0.030, 0.045, 0.060, 0.075, 0.090, 0.105, 0.120, 0.135, 0.150, 0.500,
             0.850, 0.865, 0.880, 0.895, 0.910, 0.925, 0.940, 0.955, 0.970, 0.985, 1.000]

    norms = get_norm(X_val, Y_val, U_val, V_val, X, Y, U_profile, V_profile)

    ax[2].plot(U_val, Y_val, '--o', color='orangered', label="Numerical Solutions, ref values")
    ax[3].plot(X_val, V_val, '--o', color='orangered', label="Numerical Solutions, ref values")
    ax[2].set_title("Horizontal velocity profile\nResidual: {:.2E}".format(Decimal(norms[0])))
    ax[3].set_title("Vertical velocity profile\nResidual: {:.2E}".format(Decimal(norms[1])))

if (int(Re) == 1000):
    U_val = [0.0000, -0.0757, -0.1392, -0.1951, -0.2472, -0.2960, -0.3381, -0.3690, -0.3854, -0.3869, -0.3756, -0.0620,
             0.3838, 0.3913, 0.3993, 0.4101, 0.4276, 0.4582, 0.5102, 0.5917, 0.7065, 0.8486, 1.0000]
    V_val = [0.0000, 0.1019, 0.1792, 0.2349, 0.2746, 0.3041, 0.3273, 0.3460, 0.3605, 0.3705, 0.3756, 0.0258,
             -0.4028, -0.4407, -0.4803, -0.5132, -0.5263, -0.5052, -0.4417, -0.3400, -0.2173, -0.0973, 0.0000]

    Y_val = [0.000, 0.020, 0.040, 0.060, 0.080, 0.100, 0.120, 0.140, 0.160, 0.180, 0.200, 0.500,
             0.900, 0.910, 0.920, 0.930, 0.940, 0.950, 0.960, 0.970, 0.980, 0.990, 1.000]
    X_val = [0.000, 0.015, 0.030, 0.045, 0.060, 0.075, 0.090, 0.105, 0.120, 0.135, 0.150, 0.500,
             0.850, 0.865, 0.880, 0.895, 0.910, 0.925, 0.940, 0.955, 0.970, 0.985, 1.000]

    norms = get_norm(X_val, Y_val, U_val, V_val, X, Y, U_profile, V_profile)

    ax[2].plot(U_val, Y_val, '--o', color='orangered', label="Numerical Solutions, ref values")
    ax[3].plot(X_val, V_val, '--o', color='orangered', label="Numerical Solutions, ref values")
    ax[2].set_title("Horizontal velocity profile\nResidual: {:.2E}".format(Decimal(norms[0])))
    ax[3].set_title("Vertical velocity profile\nResidual: {:.2E}".format(Decimal(norms[1])))


#print(U_res)
#print(V_res)


# Figure's titles, labels
fig.suptitle("Re = {}, grid_size = {}x{}\ninitial dt = {}, max timesteps = {}".format(Re, N, N, dt, n_iters))

if (int(Re) != 1000 and int(Re) != 2500 and int(Re) != 400):
    ax[2].set_title('Horizontal velocity profile')
    ax[3].set_title('Vertical velocity profile')

ax[0].set_xlabel('x')
ax[0].set_ylabel('y')
ax[0].set_xlim([0, 1])
ax[0].set_ylim([0, 1])

ax[1].set_xlabel('x')
ax[1].set_ylabel('y')
ax[1].set_xlim([0, 1])
ax[1].set_ylim([0, 1])

ax[2].set_xlabel('U')
ax[2].set_ylabel('y')
ax[2].set_xlim([-0.6, 1])
ax[2].set_ylim([0, 1])
ax[2].set_xticks(np.arange(-0.6, 1.01, 0.2))
ax[2].set_yticks(np.arange(0, 1.01, 0.1))

ax[3].set_xlabel('x')
ax[3].set_ylabel('V')
ax[3].set_xlim([0, 1])
ax[3].set_ylim([-0.7, 0.7])
ax[3].set_xticks(np.arange(0, 1.01, 0.1))
ax[3].set_yticks(np.arange(-0.7, 0.71, 0.1))

ax[2].grid()
ax[3].grid()

ax[2].legend()
ax[3].legend()

fig.set_figwidth(24)
fig.set_figheight(6)

plt.subplots_adjust(top=0.8)

plt.show()
plt.savefig('result.png')
