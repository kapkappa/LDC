#include "field.h"

#include <iomanip>

void Field::print(const std::string& txt) const {
    std::cout << "Field: " << txt << std::endl;
    if (array == nullptr || !initialized) {
        std::cout << "Empty field" << std::endl;
        return;
    }

    std::cout << std::scientific << std::setprecision(3);
    for (int j = y_size-1; j >= 0; j--) {
        for (unsigned int i = 0; i < x_size; i++) {
            std::cout << array[j * x_size + i] << ' ';
        }
        std::cout << std::endl;
    }
}


Field& Field::operator= (const Field& T) {
    if (this == &T)
        return *this;

    if (array == nullptr) {
        x_size = T.x_size;
        y_size = T.y_size;
        total_size = T.total_size;
        array = (double*)calloc(total_size, sizeof(double));
        initialized = true;
    }
    assert(total_size == T.total_size);
    for (unsigned int i = 0; i < total_size; i++) {
        array[i] = T[i];
    }

    return *this;
}

double& Field::operator[] (int pos) {
    assert(pos < total_size && pos >= 0);
    assert(initialized);
    return array[pos];
}

const double& Field::operator[] (int pos) const {
    assert(pos < total_size && pos >= 0);
    assert(initialized);
    return array[pos];
}


Field operator+ (const Field& A, const Field& B) {
    assert(A.total_size == B.total_size);
    Field R(A);
    for (unsigned int i = 0; i < B.total_size; i++) {
        R.array[i] += B.array[i];
    }
    return R;
}

