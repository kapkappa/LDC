#pragma once

#include "field.h"

#include <string>
#include <vector>
#include <stdio.h>

namespace logging {

void write_vertical_profile(const Field& F, const std::string& filename, double time) {
    FILE* file = fopen(filename.c_str(), "w");
    double nx = (double)F.get_x_size();
    double ny = (double)F.get_y_size();

    std::vector<double> profile(nx);
    for (int i = 0; i < nx; i++) {
        profile[i] =  F[i + ny/2 * nx];
    }

    fwrite(&time, sizeof(double), 1, file);
    fwrite(&nx, sizeof(double), 1, file);
    fwrite(profile.data(), sizeof(double), nx, file);

    fclose(file);
}

void write_horizontal_profile(const Field& F, const std::string& filename, double time) {
    FILE* file = fopen(filename.c_str(), "w");
    double nx = (double)F.get_x_size();
    double ny = (double)F.get_y_size();

    std::vector<double> profile(ny);
    for (int j = 0; j < ny; j++) {
        profile[j] =  F[nx/2 + nx * j];
    }

    fwrite(&time, sizeof(double), 1, file);
    fwrite(&ny, sizeof(double), 1, file);
    fwrite(profile.data(), sizeof(double), ny, file);

    fclose(file);
}

void write_field(const Field& F, const std::string& filename, double time) {
    FILE *file = fopen(filename.c_str(), "w");

    double nx = (double)F.get_x_size();
    double ny = (double)F.get_y_size();

    fwrite(&time, sizeof(double), 1, file);
    fwrite(&ny, sizeof(double), 1, file);
    fwrite(&nx, sizeof(double), 1, file);
    fwrite(F.get_ptr(), sizeof(double), F.get_total_size(), file);

    fclose(file);
}

}
