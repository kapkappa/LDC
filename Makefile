TARGETS = program
BASEPATH = XAMG

include $(BASEPATH)/Makefile.base

program: main.o solver.o field.o $(XAMG_SEP_LIB)
	$(MPICXX) main.o solver.o field.o -o prog $(LDFLAGS) $(LIBS)

clean::
	rm -f prog *.o U V log *.png profiles/*
