#pragma once

#include "field.h"

class Solver {
public:
    int Re;
    int N;
    double dt;
    int max_timesteps;

    double dx, dy;

    Field U, V, P;
    Field U_inaccurate, V_inaccurate;
    Field U_next, V_next;
    Field U_star, V_star;
    Field U_extra, V_extra;

    Solver(int _Re, int _N, double _dt, int _max_timesteps) : Re(_Re), N(_N), dt(_dt), max_timesteps(_max_timesteps) {
        dx = 1.0 / N;
        dy = 1.0 / N;
    }

    ~Solver() {}

    void init_psolver();
    void finalize_psolver();
    void initialize();

    void calculate_U_star(const Field&, const Field&);
    void calculate_V_star(const Field&, const Field&);

    void calculate_U_extra();
    void calculate_V_extra();

    double Conv_x (int, int, const Field&, const Field&) const;
    double Conv_y (int, int, const Field&, const Field&) const;
    double Visc_x (int, int, const Field&) const;
    double Visc_y (int, int, const Field&) const;

    void solve();
};
