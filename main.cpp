#include "solver.h"

#include <sys/time.h>
#include <string>
#include <iostream>

void show_help() {
    std::cout << "Need args: Re N dt max_timesteps" << std::endl;
}

static inline double timer() {
    struct timeval tp;
    struct timezone tzp;

    gettimeofday(&tp, &tzp);
    return ((double)tp.tv_sec + (double)tp.tv_usec * 1.e-6);
}

int main(int argc, char** argv) {

    if (argc != 5) {
        show_help();
        exit(0);
    }

    int Re = atoi(argv[1]);
    int N = atoi(argv[2]);
    double dt = std::stod(argv[3]);
    int max_timesteps = atoi(argv[4]);

    std::cout << "params: " << std::endl <<
        "Re: " << Re << std::endl <<
        "N: " << N << std::endl <<
        "dt: " << dt << std::endl <<
        "max timesteps: " << max_timesteps << std::endl;

    Solver solver(Re, N, dt, max_timesteps);
    solver.init_psolver();

    double t1, t2, t3;

    t1 = timer();
    solver.initialize();
    t2 = timer();
    solver.solve();
    t3 = timer();

    std::cout << "Initializing time: " << t2-t1 << " sec" << std::endl;
    std::cout << "Solving time: " << t3-t2 << " sec" << std::endl;

    solver.finalize_psolver();

    return 0;
}
