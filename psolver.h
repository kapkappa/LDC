#pragma once

#include <cmath>
#include <iostream>

#include "field.h"

#include "XAMG/src/xamg_headers.h"
#include "XAMG/src/xamg_types.h"

#include "XAMG/src/blas/blas.h"
#include "XAMG/src/blas2/blas2.h"
#include "XAMG/src/solvers/solver.h"

#include "XAMG/src/io/io.h"
#include "XAMG/src/part/part.h"

#include "XAMG/examples/common/generator/generator.h"

#ifndef XAMG_NV
#define XAMG_NV 1
#endif

class Psolver {
private:
    int N;
    double dx, dy;

    XAMG::matrix::matrix mat;
    XAMG::vector::vector x, b;

    std::shared_ptr<XAMG::solver::base_solver<double, 1>> solver;

    void store_result(Field& P) const {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                P[i * N + j] = x.get_value<double>(i * N + j);
            }
        }
    }


    void create_RHS(const Field& U, const Field& V, double dt) {
        double coef = dx / dt;
        for (int j = 0; j < N; j++) {
            for (int i = 0; i < N; i++) {
                double value = U[i+1 + j*(N+1)] - U[i + j*(N+1)] + V[i + (j+1)*N] - V[i + j*N];
                b.set_value<double>(i+j*N, -1.0 * value * coef);
            }
        }
    }


    void create_matrix() {
        XAMG::vector::vector x0(XAMG::mem::LOCAL);
        XAMG::vector::vector b0(XAMG::mem::LOCAL);
        x0.alloc<double>(N*N, 1);
        b0.alloc<double>(N*N, 1);
        XAMG::blas::set_const<double, 1>(x0, 0.0, true);
        XAMG::blas::set_const<double, 1>(b0, 0.0, true);

        using csr_matrix_t = XAMG::matrix::csr_matrix<double, uint32_t, uint32_t, uint32_t, uint32_t>;
        csr_matrix_t mat_csr;
        mat_csr.nrows = N * N;
        mat_csr.block_nrows = N * N;
        mat_csr.ncols = N * N;
        mat_csr.block_ncols = N * N;
        mat_csr.block_row_offset = 0;
        mat_csr.block_col_offset = 0;
        mat_csr.nonzeros = (N - 2) * (N - 2) * 5 + 4 * (N - 2) * 4 + 4 * 3 - 4;
        mat_csr.alloc();

        std::vector<double> vals(5);
        std::vector<uint64_t> cols(5);

        uint32_t cntr = 0;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (i == 1 && j == 2) {
                    vals[0] = 1.0;
                    cols[0] = i*N+j;
                    mat_csr.upload_row(i*N+j, cols, vals, 1);
                    continue;
                }

                cntr = 0;

                if (i != 0) {
                    vals[cntr] = -1.0;
                    cols[cntr++] = (i-1) * N + j;
                }
                if (j != 0) {
                    vals[cntr] = -1.0;
                    cols[cntr++] = i * N + j - 1;
                }
                if ((i == 0 && j == 0) || (i == 0 && j == N-1) || (i == N-1 && j == 0) || (i == N-1 && j == N-1))
                    vals[cntr] = 2.0;
                else if (i == 0 || j == 0 || i == N-1 || j == N-1)
                    vals[cntr] = 3.0;
                else
                    vals[cntr] = 4.0;
                cols[cntr++] = i * N + j;
                if (j != N-1) {
                    vals[cntr] = -1.0;
                    cols[cntr++] = i * N + j + 1;
                }
                if (i != N-1) {
                    vals[cntr] = -1.0;
                    cols[cntr++] = (i+1) * N + j;
                }

                mat_csr.upload_row(i * N + j, cols, vals, cntr);

            }
        }

//        mat_csr.sort();
        std::shared_ptr<XAMG::part::partitioning> part = XAMG::part::make_partitioning(mat_csr.nrows, XAMG::part::SEGMENT_BY_BLOCKS);
        mat.pack(part, mat_csr, XAMG::matrix::MPISHM_BY_BLOCKS);

        XAMG::vector::construct_distributed<double, 1>(part, x0, x);
        XAMG::vector::construct_distributed<double, 1>(part, b0, b);
    }


    void create_solver() {
        XAMG::params::global_param_list params;
        params.add_map("solver", {{"method", "PBiCGStab"},
                                  {"max_iters", "20"},
                                  {"convergence_details","check-L2,noprint"},
                                  {"rel_tolerance", "1e-10"},
                                  {"abs_tolerance", "1e-10"}});
        params.add_map("preconditioner", {{"method", "MultiGrid"},
                                          {"max_iters", "1"},
                                          {"hypre_log", "0"},
                                          {"mg_agg_num_levels", "2"},
                                          {"mg_coarse_matrix_size", "500"},
                                          {"mg_num_paths", "2"}});
        params.add_map("pre_smoother", {{"method", "HSGS"}, {"max_iters", "1"}});
        params.add_map("post_smoother", {{"method", "HSGS"}, {"max_iters", "1"}});

        params.set_defaults();

        solver = XAMG::solver::construct_solver_hierarchy<double, 1>(params, mat, x, b);

        params.print();
        solver->matrix_info();
    }




public:

    Psolver(int _N, double _dx, double _dy) : N(_N), dx(_dx), dy(_dy) {
        create_matrix();
        create_solver();
        std::cout << "Psolver initialized" << std::endl;
    }


    void solve(const Field& U, const Field& V, Field& P, double dt) {
        create_RHS(U, V, dt);
        XAMG::blas::set_const<double, 1>(x, 0.0, true);

        solver->solve();
#ifdef XAMG_DEBUG
        for (auto &x : solver->stats.abs_res)
            std::cout << ">> " << x << std::endl;
#endif
        auto &stats = solver->get_stats();

        if (!stats.if_converged[0]) {
            std::cout << "PRESSURE UNSOLVED" << std::endl;
            exit(1);
        }

        store_result(P);
    }

};
